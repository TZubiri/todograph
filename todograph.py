'''
Todograph is a free application for planning and scheduling projects.
Copyright (C) 2018 Tomas Zubiri

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License v3 as published by
the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see https://www.gnu.org/licenses/gpl-3.0.en.html

You can contact me at me@tomaszubiri.com
'''

import arango
import time
import pprint
from task import Task

class Todograph():
    def __init__(self):
        self.client = arango.ArangoClient(protocol='http', host='localhost', port=8529)
        self.db = self.client.db('todograph',username='app',password='8h374hsmcmsah72830aef')
        self.pp = pprint.PrettyPrinter(4,120) 

    def createTask(self,description, estimatedTimeInHours):
        return Task(self.db.collection('tasks').insert({"name":description,"estimatedTimeInHours":estimatedTimeInHours,"creation_date" : Todograph._timestamp()},return_new = True)["new"])

    def _getTaskByKey(self,_key):
        cursor =  self.db.aql.execute("FOR task IN tasks FILTER task._key == '{}' RETURN task".format(_key))
        results = [ Task(task) for task in cursor] 
        if len(results) == 0:
            raise Exception("There is no task with the following key: "+ _key)
        return results[0]


    def _start(self,_key):
        #TODO: Add a check that guarantees no 2 tasks are open at the same time
        activeTask = self.activeTask()
        if(activeTask):
            raise Exception("Cannot start to tasks at the same time, finish the following task first: " + activeTask)
        self.db.collection('tasks').update({"_key":_key,"start_date": Todograph._timestamp()})

    def _finish(self,_key):
        self.db.collection('tasks').update({"_key":_key,"end_date": Todograph._timestamp()})

    def _listTasks(self,filters = None):
        query_string = "FOR task IN tasks "
        if filters == None:
            pass
        if filters == "open":
            query_string += "FILTER NOT task.end_date"
        if filters == "closed":
            query_string += "FILTER task.end_date"
        
        query_string += " SORT task.priority, task.creation_date RETURN task"
        results = [Task(task) for task in self.db.aql.execute(query_string)]
        return results

    def listAllTasks(self):
        for t in self._listTasks():
            print(t)

    def listOpenTasks(self):
        for t in self._listTasks("open"):
            print(t)

    def listClosedTasks(self):
        for t in self._listTasks("closed"):
            print(t)

    def activeTask(self):
        cursor = self.db.aql.execute("FOR task IN tasks FILTER task.start_date && NOT task.end_date LIMIT 2 RETURN task")
        active_tasks = [task for task in cursor]
        if len(active_tasks) == 0:
            return None
        if len(active_tasks) == 1:
            return Task(active_tasks[0])
        if len(active_tasks) > 1:
            raise Exception("Corrupt state of database, there should never be more than one active task.")
       

    def _increasePriority(self,_key,weight = 1):
        document = self.db.collection('tasks').get({"_key": _key})
        if document == None:
            raise Exception("There is no document with the following key:" + _key)
        if "priority" in document:
            previous_priority = document["priority"]
        else:
            previous_priority = 0

        self.db.collection('tasks').update({"_key":_key,"priority": previous_priority + weight})

    def _timestamp():
        return int(time.time()*1000)

    def taskDependsOn(self, parent_task:Task,child_task:Task):
        parent_task_id = parent_task.document['_id']
        child_task_id = child_task.document['_id']
        self.db.aql.execute("INSERT {{_from:'{0}', _to:'{1}'}} IN Dependencies".format(parent_task_id, child_task_id))
