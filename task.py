'''
Todograph is a free application for planning and scheduling projects.
Copyright (C) 2018 Tomas Zubiri

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License v3 as published by
the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see https://www.gnu.org/licenses/gpl-3.0.en.html

You can contact me at me@tomaszubiri.com
'''

import datetime
import time

class Task():
    def __init__(self,document):
        self.document = document

    def __str__(self):
        output = "_key              : " + self.document["_key"] + "\n"
        output += "Description      : " +self.document["name"] + "\n"
        output += "Estimated time   : " + str(self.document["estimatedTimeInHours"])+ " hours\n"
        output += "Priority         : " + str(self.document.get("priority")) + "\n"
        if "start_date" in self.document:
            output += "Start Date       : " + datetime.datetime.fromtimestamp( self.document["start_date"] / 1000.0).strftime('%Y-%m-%d %H:%M:%S.%f') + "\n"
        if "end_date" in self.document:
            output += "End Date         : " + datetime.datetime.fromtimestamp( self.document["end_date"] / 1000.0).strftime('%Y-%m-%d %H:%M:%S.%f') + "\n"
        elapsed_time_in_ms = self.elapsed_time()
        if elapsed_time_in_ms > 0:
            elapsed_time_in_hours = elapsed_time_in_ms / (60 *60*1000)
            output += "Elapsed Time     : " + str(elapsed_time_in_hours) + " hours\n"
        return output

    def elapsed_time(self):
        start_date = self.document.get("start_date")
        end_date = self.document.get("end_date")
        if start_date == None:
            return 0
        if end_date == None:
            return time.time()*1000.0 - start_date 
        return end_date - start_date
